module github.com/introbond/lab-go-kafka

go 1.20

require github.com/confluentinc/confluent-kafka-go v1.9.2
